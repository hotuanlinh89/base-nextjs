import { combineReducers } from "redux";
import counterReducer from "../pages/counter/core/counterSlice";

export default combineReducers({
    counter: counterReducer,
});
