import type { NextApiHandler } from "next";

export const countHandler: NextApiHandler = async (request, response) => {
  const { amount = 1 } = request.body;

  // simulate IO latency
  await new Promise((resolve) => setTimeout(resolve, 500));

  response.json({ data: amount });
};

export async function fetchCount(amount = 1): Promise<{ data: number }> {
  // const response = await fetch("/api/counter", {
  //     method: "POST",
  //     headers: {
  //         "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({ amount }),
  // });
  // const result = await response.json();

  await new Promise((resolve) => setTimeout(resolve, 1500));

  const result = { data: amount };

  return result;
}
