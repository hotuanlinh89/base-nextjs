import { delay, put, takeEvery, takeLatest } from "@redux-saga/core/effects";
import { PayloadAction } from "@reduxjs/toolkit";
import { call } from "redux-saga/effects";
import { fetchCount } from "../../../api/testApi/counter";
import {
  increment,
  incrementByAmountSaga,
  incrementByAmountSagaSuccess,
} from "./counterSlice";

function* handleIncrementSaga(action: PayloadAction<number>) {
  console.log("handleIncrementSaga");
  const response: { data: number } = yield call(fetchCount, action.payload);
  yield put(incrementByAmountSagaSuccess(response.data));
}

export default function* counterSaga() {
  console.log("counter saga");
  //yield takeEvery(incrementByAmountSaga.toString(), handleIncrementSaga);
  yield takeLatest(incrementByAmountSaga.toString(), handleIncrementSaga);
}
