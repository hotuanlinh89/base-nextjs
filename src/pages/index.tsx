import type { NextPage } from 'next'
import Head from 'next/head'
import Counter from './counter/Counter'
import React from 'react'
import styled, { css } from 'styled-components'
import { defaultTheme } from 'core-ui'

const Container = styled.div`
text-align: center;

/* ${defaultTheme.device.laptop} { 
  background-color: blue;
} */
`;

const Logo = styled.img` 
  height: 100px;
  pointer-events: none;
  animation: logo-float infinite 3s ease-in-out;
  @keyframes logo-float {
    0% {
        transform: translateY(0);
    }
    50% {
        transform: translateY(10px);
    }
    100% {
        transform: translateY(0px);
    }
}

`
const Header = styled.header`
min-height: 100vh;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
font-size: calc(10px + 2vmin);
`

const Link = styled.a`
color: rgb(112, 76, 182);
`

const IndexPage: NextPage = () => {
  return (
    <Container >
      <Head>
        <title>Redux Toolkit</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header>
        <Logo src="/logo.svg" alt="logo" />
        <Counter />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <span>
          <span>Learn </span>
          <Link
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </Link>
          <span>, </span>
          <Link
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </Link>
          <span>, </span>
          <Link
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </Link>
          ,<span> and </span>
          <Link
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </Link>
        </span>
      </Header>
    </Container>
  )
}

export default IndexPage