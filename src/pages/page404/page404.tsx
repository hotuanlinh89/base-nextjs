import Link from "next/link";
import React, { memo } from "react";
import { Flex } from "core-ui";
import BoldText from "src/components/boldText/BoldText";
import styled from "styled-components";

const Container = styled(Flex)`
    background-color: red;
`;

const NoteView: React.FC = () => {
    return (
        <Container>
            <BoldText textString=" 404 page" />
        </Container>);
};

export default memo(NoteView);
