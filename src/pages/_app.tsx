import { Provider } from 'react-redux'
import type { AppProps } from 'next/app'
import store from '../redux/store'
import React from 'react'
import { ThemeProvider } from 'styled-components'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <ThemeProvider theme={{ margin: "0px", border: "0px" }}>
        <Component {...pageProps} />
      </ThemeProvider>
    </Provider>
  )
}

export default MyApp