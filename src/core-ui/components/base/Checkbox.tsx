import { Checkbox } from 'antd';
import styled from 'styled-components';
import { defaultTheme } from '../../theme';
export default styled(Checkbox)`
  color: ${defaultTheme.colors.info};
  .ant-checkbox {
    color: ${defaultTheme.colors.info};
  }

  .ant-checkbox-inner {
    border: 1px solid #3f748dee;
  }
`;
