import { Button } from 'antd';
import { ButtonProps as AntdButtonProps } from 'antd/lib/button';
import styled from 'styled-components';
import { defaultTheme } from '../../theme';

type ButtonTypes =
  | 'default'
  | 'primary'
  | 'secondary'
  | 'ghost'
  | 'dashed'
  | 'danger'
  | 'link';

// @ts-ignore
export interface ButtonProps extends AntdButtonProps {
  type: ButtonTypes;
  color: string;
}

// @ts-ignore
export default styled<ButtonProps>(Button)`
  background-color: ${(props: any) =>
    defaultTheme.colors[props.color]} !important;
  color: ${(props: any) =>
    !!props.color && props.color !== 'white' ? 'white' : ''};
  &:hover {
    color: ${(props: any) => (!!props.color ? 'white' : '')};
  }
  opacity: ${(props: any) => (props.disabled ? 0.6 : 1)};
  & span {
    color: ${(props: any) =>
    !!props.color && props.color !== 'white' ? 'white' : ''};
  }
` as any;
