import styled from 'styled-components';

const Flex = styled.div({
  display: 'flex',
}) as React.FunctionComponent<any>;

export default Flex;
