import { FunctionComponent } from 'react';
import { Checkbox } from 'antd';
import { CheckboxGroupProps as AntdCheckboxGroupProps } from 'antd/lib/checkbox';
import styled from 'styled-components';

export interface CheckboxGroupProps extends AntdCheckboxGroupProps {
  direction?: 'column' | 'row';
  itemWidth?: string | number;
}

//@ts-ignore
const CheckBoxGroup = styled<CheckboxGroupProps>(Checkbox.Group)`
  ${({ direction, itemWidth }) => {
    if (direction === 'column')
      return `
      display: flex !important;
      flex-direction: column !important;
      > .ant-checkbox-wrapper {
        padding-top: 7px;
        color: #3f748d;
  }
    `;
    else
      return `
      display: flex;
      flex-wrap: wrap;
      > .ant-checkbox-group-item {
        ${itemWidth && `width: ${itemWidth}`};
        color: #3f748d;
        margin-right: 0px;
        padding-right: 8px;
        padding-top: 5px;
      }
       > .ant-checkbox-wrapper {
        color: #3f748d;
  }
    `;
  }}
`;

//@ts-ignore
CheckBoxGroup.defaultProps = {
  direction: 'row',
};

export default CheckBoxGroup as any;
