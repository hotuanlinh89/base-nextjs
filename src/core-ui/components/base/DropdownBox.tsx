import styled from 'styled-components';
import Flex from './Flex';

const DropdownBox = styled(Flex)`
  color: ${({ disabled = false, theme }) =>
    disabled ? theme.colors.disabled : theme.colors.textPrimary};
  background-color: ${({ disabled = false, theme }) =>
    disabled ? theme.colors.disabledBackground : 'inherit'};
  padding-left: 2;
  padding-right: 2;
  padding-top: 5px;
  padding-bottom: 5px;
  border: ${({ border = true }) => (!border ? '' : '1px solid #dddddd')};
  border-radius: 4px;
  align-items: center;
  cursor: ${({ disabled = false }) => (disabled ? 'not-allowed' : 'pointer')};
  &:hover {
    border: ${({ border = true, disabled = false }) =>
    !border || disabled ? '' : '1px solid #3f748dee'};
  }
  transition: 0.3s;
`;

export default DropdownBox;
