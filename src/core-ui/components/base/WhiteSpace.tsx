import React from 'react';
import styled from 'styled-components';
import { defaultTheme } from 'core-ui';


const WhiteSpaceHorizontal = styled.div<ButtonProps>`
width:${(props) => defaultTheme.space[props.size]};
`;

const WhiteSpaceVertical = styled.div<ButtonProps>`
height:${(props) => defaultTheme.space[props.size]};
`;


interface ButtonProps {
  size: number,
  horizontal: boolean,
}

const WhiteSpace: React.FC<ButtonProps> = (props) => {

  const { horizontal, size } = props;

  //return <div style={{ padding: horizontal ? `0 ${defaultTheme.space[size]} 0 ${defaultTheme.space[size]}` : `${defaultTheme.space[size]} 0 ${defaultTheme.space[size]} 0` }} />;

  return horizontal ? <WhiteSpaceHorizontal size={size} horizontal={horizontal} /> : <WhiteSpaceVertical size={size} horizontal={horizontal} />;


}

export default WhiteSpace;
