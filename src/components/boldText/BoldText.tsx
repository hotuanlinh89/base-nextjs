import React, { FC, memo } from "react";
import styled from "styled-components";

const CustomBoldText = styled.div`
    font-weight: bold;
`;
interface BoldTextProps {
    textString: string;
}
const BoldText: FC<BoldTextProps> = (props) => {
    const { textString } = props;
    return (
        <CustomBoldText >
            {textString}
        </CustomBoldText>
    );
};
export default BoldText;


